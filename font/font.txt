The static website (about.gitlab.com) uses Roboto Condensed in 700 (bold), 400 (regular), and 300 (light) weights. 

The GitLab logo is custom lettering done by designer Ty Wilkins that can be found in gitlab-artwork. The fonts suggested by Ty to be used in conjunction to the custom lettering are Dobra (https://www.myfonts.com/fonts/dstype/dobra) and FF Clan Pro (https://www.myfonts.com/fonts/fontfont/clan-pro)

The GitLab application uses the standard Helvetica Neue font.

